#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "stdio.h"

void vApplicationIdleHook(void) {
}

void vMainQueueSendPassed(void) {
}

void mytask(void *p) {
  const portTickType ticks = 5000 / portTICK_RATE_MS;
  for (;;) {
    printf("hello\n");
    vTaskDelay(ticks);
  }
}

int main() {
  xTaskCreate(mytask, "hello", 512, NULL, 2, NULL);
  vTaskStartScheduler();
}
