#!/bin/bash
INC='-I. -I./FreeRTOS_Kernel/include -I./FreeRTOS_Kernel/portable/GCC/Posix'
LIB='-lpthread -lrt'

gcc $INC -c FreeRTOS_Kernel/*.c
gcc $INC -c FreeRTOS_Kernel/portable/MemMang/*.c
gcc $INC -c FreeRTOS_Kernel/portable/GCC/Posix/*.c
gcc $INC -c main.c

gcc $LIB -o d *.o
